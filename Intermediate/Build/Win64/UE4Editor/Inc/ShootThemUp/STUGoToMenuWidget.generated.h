// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SHOOTTHEMUP_STUGoToMenuWidget_generated_h
#error "STUGoToMenuWidget.generated.h already included, missing '#pragma once' in STUGoToMenuWidget.h"
#endif
#define SHOOTTHEMUP_STUGoToMenuWidget_generated_h

#define shooter_Source_ShootThemUp_Public_UI_STUGoToMenuWidget_h_14_SPARSE_DATA
#define shooter_Source_ShootThemUp_Public_UI_STUGoToMenuWidget_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnGoToMenu);


#define shooter_Source_ShootThemUp_Public_UI_STUGoToMenuWidget_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnGoToMenu);


#define shooter_Source_ShootThemUp_Public_UI_STUGoToMenuWidget_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSTUGoToMenuWidget(); \
	friend struct Z_Construct_UClass_USTUGoToMenuWidget_Statics; \
public: \
	DECLARE_CLASS(USTUGoToMenuWidget, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ShootThemUp"), NO_API) \
	DECLARE_SERIALIZER(USTUGoToMenuWidget)


#define shooter_Source_ShootThemUp_Public_UI_STUGoToMenuWidget_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUSTUGoToMenuWidget(); \
	friend struct Z_Construct_UClass_USTUGoToMenuWidget_Statics; \
public: \
	DECLARE_CLASS(USTUGoToMenuWidget, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ShootThemUp"), NO_API) \
	DECLARE_SERIALIZER(USTUGoToMenuWidget)


#define shooter_Source_ShootThemUp_Public_UI_STUGoToMenuWidget_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USTUGoToMenuWidget(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USTUGoToMenuWidget) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USTUGoToMenuWidget); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USTUGoToMenuWidget); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USTUGoToMenuWidget(USTUGoToMenuWidget&&); \
	NO_API USTUGoToMenuWidget(const USTUGoToMenuWidget&); \
public:


#define shooter_Source_ShootThemUp_Public_UI_STUGoToMenuWidget_h_14_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USTUGoToMenuWidget(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USTUGoToMenuWidget(USTUGoToMenuWidget&&); \
	NO_API USTUGoToMenuWidget(const USTUGoToMenuWidget&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USTUGoToMenuWidget); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USTUGoToMenuWidget); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USTUGoToMenuWidget)


#define shooter_Source_ShootThemUp_Public_UI_STUGoToMenuWidget_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__GoToMenuButton() { return STRUCT_OFFSET(USTUGoToMenuWidget, GoToMenuButton); }


#define shooter_Source_ShootThemUp_Public_UI_STUGoToMenuWidget_h_11_PROLOG
#define shooter_Source_ShootThemUp_Public_UI_STUGoToMenuWidget_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	shooter_Source_ShootThemUp_Public_UI_STUGoToMenuWidget_h_14_PRIVATE_PROPERTY_OFFSET \
	shooter_Source_ShootThemUp_Public_UI_STUGoToMenuWidget_h_14_SPARSE_DATA \
	shooter_Source_ShootThemUp_Public_UI_STUGoToMenuWidget_h_14_RPC_WRAPPERS \
	shooter_Source_ShootThemUp_Public_UI_STUGoToMenuWidget_h_14_INCLASS \
	shooter_Source_ShootThemUp_Public_UI_STUGoToMenuWidget_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define shooter_Source_ShootThemUp_Public_UI_STUGoToMenuWidget_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	shooter_Source_ShootThemUp_Public_UI_STUGoToMenuWidget_h_14_PRIVATE_PROPERTY_OFFSET \
	shooter_Source_ShootThemUp_Public_UI_STUGoToMenuWidget_h_14_SPARSE_DATA \
	shooter_Source_ShootThemUp_Public_UI_STUGoToMenuWidget_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	shooter_Source_ShootThemUp_Public_UI_STUGoToMenuWidget_h_14_INCLASS_NO_PURE_DECLS \
	shooter_Source_ShootThemUp_Public_UI_STUGoToMenuWidget_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SHOOTTHEMUP_API UClass* StaticClass<class USTUGoToMenuWidget>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID shooter_Source_ShootThemUp_Public_UI_STUGoToMenuWidget_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
