// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SHOOTTHEMUP_STULevelItemWidget_generated_h
#error "STULevelItemWidget.generated.h already included, missing '#pragma once' in STULevelItemWidget.h"
#endif
#define SHOOTTHEMUP_STULevelItemWidget_generated_h

#define shooter_Source_ShootThemUp_Public_Menu_UI_STULevelItemWidget_h_17_SPARSE_DATA
#define shooter_Source_ShootThemUp_Public_Menu_UI_STULevelItemWidget_h_17_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnLevelItemClicked);


#define shooter_Source_ShootThemUp_Public_Menu_UI_STULevelItemWidget_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnLevelItemClicked);


#define shooter_Source_ShootThemUp_Public_Menu_UI_STULevelItemWidget_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSTULevelItemWidget(); \
	friend struct Z_Construct_UClass_USTULevelItemWidget_Statics; \
public: \
	DECLARE_CLASS(USTULevelItemWidget, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ShootThemUp"), NO_API) \
	DECLARE_SERIALIZER(USTULevelItemWidget)


#define shooter_Source_ShootThemUp_Public_Menu_UI_STULevelItemWidget_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUSTULevelItemWidget(); \
	friend struct Z_Construct_UClass_USTULevelItemWidget_Statics; \
public: \
	DECLARE_CLASS(USTULevelItemWidget, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ShootThemUp"), NO_API) \
	DECLARE_SERIALIZER(USTULevelItemWidget)


#define shooter_Source_ShootThemUp_Public_Menu_UI_STULevelItemWidget_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USTULevelItemWidget(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USTULevelItemWidget) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USTULevelItemWidget); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USTULevelItemWidget); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USTULevelItemWidget(USTULevelItemWidget&&); \
	NO_API USTULevelItemWidget(const USTULevelItemWidget&); \
public:


#define shooter_Source_ShootThemUp_Public_Menu_UI_STULevelItemWidget_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USTULevelItemWidget(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USTULevelItemWidget(USTULevelItemWidget&&); \
	NO_API USTULevelItemWidget(const USTULevelItemWidget&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USTULevelItemWidget); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USTULevelItemWidget); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USTULevelItemWidget)


#define shooter_Source_ShootThemUp_Public_Menu_UI_STULevelItemWidget_h_17_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__LevelSelectButton() { return STRUCT_OFFSET(USTULevelItemWidget, LevelSelectButton); } \
	FORCEINLINE static uint32 __PPO__LevelNameTextBlock() { return STRUCT_OFFSET(USTULevelItemWidget, LevelNameTextBlock); } \
	FORCEINLINE static uint32 __PPO__LevelImage() { return STRUCT_OFFSET(USTULevelItemWidget, LevelImage); } \
	FORCEINLINE static uint32 __PPO__FrameImage() { return STRUCT_OFFSET(USTULevelItemWidget, FrameImage); }


#define shooter_Source_ShootThemUp_Public_Menu_UI_STULevelItemWidget_h_14_PROLOG
#define shooter_Source_ShootThemUp_Public_Menu_UI_STULevelItemWidget_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	shooter_Source_ShootThemUp_Public_Menu_UI_STULevelItemWidget_h_17_PRIVATE_PROPERTY_OFFSET \
	shooter_Source_ShootThemUp_Public_Menu_UI_STULevelItemWidget_h_17_SPARSE_DATA \
	shooter_Source_ShootThemUp_Public_Menu_UI_STULevelItemWidget_h_17_RPC_WRAPPERS \
	shooter_Source_ShootThemUp_Public_Menu_UI_STULevelItemWidget_h_17_INCLASS \
	shooter_Source_ShootThemUp_Public_Menu_UI_STULevelItemWidget_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define shooter_Source_ShootThemUp_Public_Menu_UI_STULevelItemWidget_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	shooter_Source_ShootThemUp_Public_Menu_UI_STULevelItemWidget_h_17_PRIVATE_PROPERTY_OFFSET \
	shooter_Source_ShootThemUp_Public_Menu_UI_STULevelItemWidget_h_17_SPARSE_DATA \
	shooter_Source_ShootThemUp_Public_Menu_UI_STULevelItemWidget_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	shooter_Source_ShootThemUp_Public_Menu_UI_STULevelItemWidget_h_17_INCLASS_NO_PURE_DECLS \
	shooter_Source_ShootThemUp_Public_Menu_UI_STULevelItemWidget_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SHOOTTHEMUP_API UClass* StaticClass<class USTULevelItemWidget>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID shooter_Source_ShootThemUp_Public_Menu_UI_STULevelItemWidget_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
