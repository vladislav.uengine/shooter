// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SHOOTTHEMUP_STURifleWeapon_generated_h
#error "STURifleWeapon.generated.h already included, missing '#pragma once' in STURifleWeapon.h"
#endif
#define SHOOTTHEMUP_STURifleWeapon_generated_h

#define shooter_Source_ShootThemUp_Public_Weapon_STURifleWeapon_h_17_SPARSE_DATA
#define shooter_Source_ShootThemUp_Public_Weapon_STURifleWeapon_h_17_RPC_WRAPPERS
#define shooter_Source_ShootThemUp_Public_Weapon_STURifleWeapon_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define shooter_Source_ShootThemUp_Public_Weapon_STURifleWeapon_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASTURifleWeapon(); \
	friend struct Z_Construct_UClass_ASTURifleWeapon_Statics; \
public: \
	DECLARE_CLASS(ASTURifleWeapon, ASTUBaseWeapon, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ShootThemUp"), NO_API) \
	DECLARE_SERIALIZER(ASTURifleWeapon)


#define shooter_Source_ShootThemUp_Public_Weapon_STURifleWeapon_h_17_INCLASS \
private: \
	static void StaticRegisterNativesASTURifleWeapon(); \
	friend struct Z_Construct_UClass_ASTURifleWeapon_Statics; \
public: \
	DECLARE_CLASS(ASTURifleWeapon, ASTUBaseWeapon, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ShootThemUp"), NO_API) \
	DECLARE_SERIALIZER(ASTURifleWeapon)


#define shooter_Source_ShootThemUp_Public_Weapon_STURifleWeapon_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASTURifleWeapon(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASTURifleWeapon) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASTURifleWeapon); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASTURifleWeapon); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASTURifleWeapon(ASTURifleWeapon&&); \
	NO_API ASTURifleWeapon(const ASTURifleWeapon&); \
public:


#define shooter_Source_ShootThemUp_Public_Weapon_STURifleWeapon_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASTURifleWeapon(ASTURifleWeapon&&); \
	NO_API ASTURifleWeapon(const ASTURifleWeapon&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASTURifleWeapon); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASTURifleWeapon); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASTURifleWeapon)


#define shooter_Source_ShootThemUp_Public_Weapon_STURifleWeapon_h_17_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__TimeBetweenShots() { return STRUCT_OFFSET(ASTURifleWeapon, TimeBetweenShots); } \
	FORCEINLINE static uint32 __PPO__BulletSpread() { return STRUCT_OFFSET(ASTURifleWeapon, BulletSpread); } \
	FORCEINLINE static uint32 __PPO__DamageAmount() { return STRUCT_OFFSET(ASTURifleWeapon, DamageAmount); } \
	FORCEINLINE static uint32 __PPO__TraceFX() { return STRUCT_OFFSET(ASTURifleWeapon, TraceFX); } \
	FORCEINLINE static uint32 __PPO__TraceTargetName() { return STRUCT_OFFSET(ASTURifleWeapon, TraceTargetName); } \
	FORCEINLINE static uint32 __PPO__WeaponFXComponent() { return STRUCT_OFFSET(ASTURifleWeapon, WeaponFXComponent); } \
	FORCEINLINE static uint32 __PPO__MuzzleFXComponent() { return STRUCT_OFFSET(ASTURifleWeapon, MuzzleFXComponent); } \
	FORCEINLINE static uint32 __PPO__FireAudioComponent() { return STRUCT_OFFSET(ASTURifleWeapon, FireAudioComponent); }


#define shooter_Source_ShootThemUp_Public_Weapon_STURifleWeapon_h_14_PROLOG
#define shooter_Source_ShootThemUp_Public_Weapon_STURifleWeapon_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	shooter_Source_ShootThemUp_Public_Weapon_STURifleWeapon_h_17_PRIVATE_PROPERTY_OFFSET \
	shooter_Source_ShootThemUp_Public_Weapon_STURifleWeapon_h_17_SPARSE_DATA \
	shooter_Source_ShootThemUp_Public_Weapon_STURifleWeapon_h_17_RPC_WRAPPERS \
	shooter_Source_ShootThemUp_Public_Weapon_STURifleWeapon_h_17_INCLASS \
	shooter_Source_ShootThemUp_Public_Weapon_STURifleWeapon_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define shooter_Source_ShootThemUp_Public_Weapon_STURifleWeapon_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	shooter_Source_ShootThemUp_Public_Weapon_STURifleWeapon_h_17_PRIVATE_PROPERTY_OFFSET \
	shooter_Source_ShootThemUp_Public_Weapon_STURifleWeapon_h_17_SPARSE_DATA \
	shooter_Source_ShootThemUp_Public_Weapon_STURifleWeapon_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	shooter_Source_ShootThemUp_Public_Weapon_STURifleWeapon_h_17_INCLASS_NO_PURE_DECLS \
	shooter_Source_ShootThemUp_Public_Weapon_STURifleWeapon_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SHOOTTHEMUP_API UClass* StaticClass<class ASTURifleWeapon>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID shooter_Source_ShootThemUp_Public_Weapon_STURifleWeapon_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
