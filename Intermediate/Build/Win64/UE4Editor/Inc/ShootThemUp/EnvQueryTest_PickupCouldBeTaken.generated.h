// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SHOOTTHEMUP_EnvQueryTest_PickupCouldBeTaken_generated_h
#error "EnvQueryTest_PickupCouldBeTaken.generated.h already included, missing '#pragma once' in EnvQueryTest_PickupCouldBeTaken.h"
#endif
#define SHOOTTHEMUP_EnvQueryTest_PickupCouldBeTaken_generated_h

#define shooter_Source_ShootThemUp_Public_AI_EQS_EnvQueryTest_PickupCouldBeTaken_h_15_SPARSE_DATA
#define shooter_Source_ShootThemUp_Public_AI_EQS_EnvQueryTest_PickupCouldBeTaken_h_15_RPC_WRAPPERS
#define shooter_Source_ShootThemUp_Public_AI_EQS_EnvQueryTest_PickupCouldBeTaken_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define shooter_Source_ShootThemUp_Public_AI_EQS_EnvQueryTest_PickupCouldBeTaken_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUEnvQueryTest_PickupCouldBeTaken(); \
	friend struct Z_Construct_UClass_UEnvQueryTest_PickupCouldBeTaken_Statics; \
public: \
	DECLARE_CLASS(UEnvQueryTest_PickupCouldBeTaken, UEnvQueryTest, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ShootThemUp"), NO_API) \
	DECLARE_SERIALIZER(UEnvQueryTest_PickupCouldBeTaken)


#define shooter_Source_ShootThemUp_Public_AI_EQS_EnvQueryTest_PickupCouldBeTaken_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUEnvQueryTest_PickupCouldBeTaken(); \
	friend struct Z_Construct_UClass_UEnvQueryTest_PickupCouldBeTaken_Statics; \
public: \
	DECLARE_CLASS(UEnvQueryTest_PickupCouldBeTaken, UEnvQueryTest, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ShootThemUp"), NO_API) \
	DECLARE_SERIALIZER(UEnvQueryTest_PickupCouldBeTaken)


#define shooter_Source_ShootThemUp_Public_AI_EQS_EnvQueryTest_PickupCouldBeTaken_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UEnvQueryTest_PickupCouldBeTaken(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UEnvQueryTest_PickupCouldBeTaken) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEnvQueryTest_PickupCouldBeTaken); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEnvQueryTest_PickupCouldBeTaken); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEnvQueryTest_PickupCouldBeTaken(UEnvQueryTest_PickupCouldBeTaken&&); \
	NO_API UEnvQueryTest_PickupCouldBeTaken(const UEnvQueryTest_PickupCouldBeTaken&); \
public:


#define shooter_Source_ShootThemUp_Public_AI_EQS_EnvQueryTest_PickupCouldBeTaken_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEnvQueryTest_PickupCouldBeTaken(UEnvQueryTest_PickupCouldBeTaken&&); \
	NO_API UEnvQueryTest_PickupCouldBeTaken(const UEnvQueryTest_PickupCouldBeTaken&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEnvQueryTest_PickupCouldBeTaken); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEnvQueryTest_PickupCouldBeTaken); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UEnvQueryTest_PickupCouldBeTaken)


#define shooter_Source_ShootThemUp_Public_AI_EQS_EnvQueryTest_PickupCouldBeTaken_h_15_PRIVATE_PROPERTY_OFFSET
#define shooter_Source_ShootThemUp_Public_AI_EQS_EnvQueryTest_PickupCouldBeTaken_h_12_PROLOG
#define shooter_Source_ShootThemUp_Public_AI_EQS_EnvQueryTest_PickupCouldBeTaken_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	shooter_Source_ShootThemUp_Public_AI_EQS_EnvQueryTest_PickupCouldBeTaken_h_15_PRIVATE_PROPERTY_OFFSET \
	shooter_Source_ShootThemUp_Public_AI_EQS_EnvQueryTest_PickupCouldBeTaken_h_15_SPARSE_DATA \
	shooter_Source_ShootThemUp_Public_AI_EQS_EnvQueryTest_PickupCouldBeTaken_h_15_RPC_WRAPPERS \
	shooter_Source_ShootThemUp_Public_AI_EQS_EnvQueryTest_PickupCouldBeTaken_h_15_INCLASS \
	shooter_Source_ShootThemUp_Public_AI_EQS_EnvQueryTest_PickupCouldBeTaken_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define shooter_Source_ShootThemUp_Public_AI_EQS_EnvQueryTest_PickupCouldBeTaken_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	shooter_Source_ShootThemUp_Public_AI_EQS_EnvQueryTest_PickupCouldBeTaken_h_15_PRIVATE_PROPERTY_OFFSET \
	shooter_Source_ShootThemUp_Public_AI_EQS_EnvQueryTest_PickupCouldBeTaken_h_15_SPARSE_DATA \
	shooter_Source_ShootThemUp_Public_AI_EQS_EnvQueryTest_PickupCouldBeTaken_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	shooter_Source_ShootThemUp_Public_AI_EQS_EnvQueryTest_PickupCouldBeTaken_h_15_INCLASS_NO_PURE_DECLS \
	shooter_Source_ShootThemUp_Public_AI_EQS_EnvQueryTest_PickupCouldBeTaken_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SHOOTTHEMUP_API UClass* StaticClass<class UEnvQueryTest_PickupCouldBeTaken>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID shooter_Source_ShootThemUp_Public_AI_EQS_EnvQueryTest_PickupCouldBeTaken_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
