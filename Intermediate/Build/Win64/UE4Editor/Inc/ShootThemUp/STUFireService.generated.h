// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SHOOTTHEMUP_STUFireService_generated_h
#error "STUFireService.generated.h already included, missing '#pragma once' in STUFireService.h"
#endif
#define SHOOTTHEMUP_STUFireService_generated_h

#define shooter_Source_ShootThemUp_Public_AI_Services_STUFireService_h_12_SPARSE_DATA
#define shooter_Source_ShootThemUp_Public_AI_Services_STUFireService_h_12_RPC_WRAPPERS
#define shooter_Source_ShootThemUp_Public_AI_Services_STUFireService_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define shooter_Source_ShootThemUp_Public_AI_Services_STUFireService_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSTUFireService(); \
	friend struct Z_Construct_UClass_USTUFireService_Statics; \
public: \
	DECLARE_CLASS(USTUFireService, UBTService, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ShootThemUp"), NO_API) \
	DECLARE_SERIALIZER(USTUFireService)


#define shooter_Source_ShootThemUp_Public_AI_Services_STUFireService_h_12_INCLASS \
private: \
	static void StaticRegisterNativesUSTUFireService(); \
	friend struct Z_Construct_UClass_USTUFireService_Statics; \
public: \
	DECLARE_CLASS(USTUFireService, UBTService, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ShootThemUp"), NO_API) \
	DECLARE_SERIALIZER(USTUFireService)


#define shooter_Source_ShootThemUp_Public_AI_Services_STUFireService_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USTUFireService(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USTUFireService) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USTUFireService); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USTUFireService); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USTUFireService(USTUFireService&&); \
	NO_API USTUFireService(const USTUFireService&); \
public:


#define shooter_Source_ShootThemUp_Public_AI_Services_STUFireService_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USTUFireService(USTUFireService&&); \
	NO_API USTUFireService(const USTUFireService&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USTUFireService); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USTUFireService); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(USTUFireService)


#define shooter_Source_ShootThemUp_Public_AI_Services_STUFireService_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__EnemyActorKey() { return STRUCT_OFFSET(USTUFireService, EnemyActorKey); }


#define shooter_Source_ShootThemUp_Public_AI_Services_STUFireService_h_9_PROLOG
#define shooter_Source_ShootThemUp_Public_AI_Services_STUFireService_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	shooter_Source_ShootThemUp_Public_AI_Services_STUFireService_h_12_PRIVATE_PROPERTY_OFFSET \
	shooter_Source_ShootThemUp_Public_AI_Services_STUFireService_h_12_SPARSE_DATA \
	shooter_Source_ShootThemUp_Public_AI_Services_STUFireService_h_12_RPC_WRAPPERS \
	shooter_Source_ShootThemUp_Public_AI_Services_STUFireService_h_12_INCLASS \
	shooter_Source_ShootThemUp_Public_AI_Services_STUFireService_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define shooter_Source_ShootThemUp_Public_AI_Services_STUFireService_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	shooter_Source_ShootThemUp_Public_AI_Services_STUFireService_h_12_PRIVATE_PROPERTY_OFFSET \
	shooter_Source_ShootThemUp_Public_AI_Services_STUFireService_h_12_SPARSE_DATA \
	shooter_Source_ShootThemUp_Public_AI_Services_STUFireService_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	shooter_Source_ShootThemUp_Public_AI_Services_STUFireService_h_12_INCLASS_NO_PURE_DECLS \
	shooter_Source_ShootThemUp_Public_AI_Services_STUFireService_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SHOOTTHEMUP_API UClass* StaticClass<class USTUFireService>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID shooter_Source_ShootThemUp_Public_AI_Services_STUFireService_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
