// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SHOOTTHEMUP_STUNeedAmmoDecorator_generated_h
#error "STUNeedAmmoDecorator.generated.h already included, missing '#pragma once' in STUNeedAmmoDecorator.h"
#endif
#define SHOOTTHEMUP_STUNeedAmmoDecorator_generated_h

#define shooter_Source_ShootThemUp_Public_AI_Decorators_STUNeedAmmoDecorator_h_13_SPARSE_DATA
#define shooter_Source_ShootThemUp_Public_AI_Decorators_STUNeedAmmoDecorator_h_13_RPC_WRAPPERS
#define shooter_Source_ShootThemUp_Public_AI_Decorators_STUNeedAmmoDecorator_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define shooter_Source_ShootThemUp_Public_AI_Decorators_STUNeedAmmoDecorator_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSTUNeedAmmoDecorator(); \
	friend struct Z_Construct_UClass_USTUNeedAmmoDecorator_Statics; \
public: \
	DECLARE_CLASS(USTUNeedAmmoDecorator, UBTDecorator, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ShootThemUp"), NO_API) \
	DECLARE_SERIALIZER(USTUNeedAmmoDecorator)


#define shooter_Source_ShootThemUp_Public_AI_Decorators_STUNeedAmmoDecorator_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUSTUNeedAmmoDecorator(); \
	friend struct Z_Construct_UClass_USTUNeedAmmoDecorator_Statics; \
public: \
	DECLARE_CLASS(USTUNeedAmmoDecorator, UBTDecorator, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ShootThemUp"), NO_API) \
	DECLARE_SERIALIZER(USTUNeedAmmoDecorator)


#define shooter_Source_ShootThemUp_Public_AI_Decorators_STUNeedAmmoDecorator_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USTUNeedAmmoDecorator(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USTUNeedAmmoDecorator) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USTUNeedAmmoDecorator); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USTUNeedAmmoDecorator); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USTUNeedAmmoDecorator(USTUNeedAmmoDecorator&&); \
	NO_API USTUNeedAmmoDecorator(const USTUNeedAmmoDecorator&); \
public:


#define shooter_Source_ShootThemUp_Public_AI_Decorators_STUNeedAmmoDecorator_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USTUNeedAmmoDecorator(USTUNeedAmmoDecorator&&); \
	NO_API USTUNeedAmmoDecorator(const USTUNeedAmmoDecorator&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USTUNeedAmmoDecorator); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USTUNeedAmmoDecorator); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(USTUNeedAmmoDecorator)


#define shooter_Source_ShootThemUp_Public_AI_Decorators_STUNeedAmmoDecorator_h_13_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__WeaponType() { return STRUCT_OFFSET(USTUNeedAmmoDecorator, WeaponType); }


#define shooter_Source_ShootThemUp_Public_AI_Decorators_STUNeedAmmoDecorator_h_10_PROLOG
#define shooter_Source_ShootThemUp_Public_AI_Decorators_STUNeedAmmoDecorator_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	shooter_Source_ShootThemUp_Public_AI_Decorators_STUNeedAmmoDecorator_h_13_PRIVATE_PROPERTY_OFFSET \
	shooter_Source_ShootThemUp_Public_AI_Decorators_STUNeedAmmoDecorator_h_13_SPARSE_DATA \
	shooter_Source_ShootThemUp_Public_AI_Decorators_STUNeedAmmoDecorator_h_13_RPC_WRAPPERS \
	shooter_Source_ShootThemUp_Public_AI_Decorators_STUNeedAmmoDecorator_h_13_INCLASS \
	shooter_Source_ShootThemUp_Public_AI_Decorators_STUNeedAmmoDecorator_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define shooter_Source_ShootThemUp_Public_AI_Decorators_STUNeedAmmoDecorator_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	shooter_Source_ShootThemUp_Public_AI_Decorators_STUNeedAmmoDecorator_h_13_PRIVATE_PROPERTY_OFFSET \
	shooter_Source_ShootThemUp_Public_AI_Decorators_STUNeedAmmoDecorator_h_13_SPARSE_DATA \
	shooter_Source_ShootThemUp_Public_AI_Decorators_STUNeedAmmoDecorator_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	shooter_Source_ShootThemUp_Public_AI_Decorators_STUNeedAmmoDecorator_h_13_INCLASS_NO_PURE_DECLS \
	shooter_Source_ShootThemUp_Public_AI_Decorators_STUNeedAmmoDecorator_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SHOOTTHEMUP_API UClass* StaticClass<class USTUNeedAmmoDecorator>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID shooter_Source_ShootThemUp_Public_AI_Decorators_STUNeedAmmoDecorator_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
