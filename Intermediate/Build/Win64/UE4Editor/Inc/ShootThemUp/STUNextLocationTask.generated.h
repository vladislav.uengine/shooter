// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SHOOTTHEMUP_STUNextLocationTask_generated_h
#error "STUNextLocationTask.generated.h already included, missing '#pragma once' in STUNextLocationTask.h"
#endif
#define SHOOTTHEMUP_STUNextLocationTask_generated_h

#define shooter_Source_ShootThemUp_Public_AI_Tasks_STUNextLocationTask_h_12_SPARSE_DATA
#define shooter_Source_ShootThemUp_Public_AI_Tasks_STUNextLocationTask_h_12_RPC_WRAPPERS
#define shooter_Source_ShootThemUp_Public_AI_Tasks_STUNextLocationTask_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define shooter_Source_ShootThemUp_Public_AI_Tasks_STUNextLocationTask_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSTUNextLocationTask(); \
	friend struct Z_Construct_UClass_USTUNextLocationTask_Statics; \
public: \
	DECLARE_CLASS(USTUNextLocationTask, UBTTaskNode, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ShootThemUp"), NO_API) \
	DECLARE_SERIALIZER(USTUNextLocationTask)


#define shooter_Source_ShootThemUp_Public_AI_Tasks_STUNextLocationTask_h_12_INCLASS \
private: \
	static void StaticRegisterNativesUSTUNextLocationTask(); \
	friend struct Z_Construct_UClass_USTUNextLocationTask_Statics; \
public: \
	DECLARE_CLASS(USTUNextLocationTask, UBTTaskNode, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ShootThemUp"), NO_API) \
	DECLARE_SERIALIZER(USTUNextLocationTask)


#define shooter_Source_ShootThemUp_Public_AI_Tasks_STUNextLocationTask_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USTUNextLocationTask(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USTUNextLocationTask) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USTUNextLocationTask); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USTUNextLocationTask); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USTUNextLocationTask(USTUNextLocationTask&&); \
	NO_API USTUNextLocationTask(const USTUNextLocationTask&); \
public:


#define shooter_Source_ShootThemUp_Public_AI_Tasks_STUNextLocationTask_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USTUNextLocationTask(USTUNextLocationTask&&); \
	NO_API USTUNextLocationTask(const USTUNextLocationTask&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USTUNextLocationTask); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USTUNextLocationTask); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(USTUNextLocationTask)


#define shooter_Source_ShootThemUp_Public_AI_Tasks_STUNextLocationTask_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Radius() { return STRUCT_OFFSET(USTUNextLocationTask, Radius); } \
	FORCEINLINE static uint32 __PPO__AimLocationKey() { return STRUCT_OFFSET(USTUNextLocationTask, AimLocationKey); } \
	FORCEINLINE static uint32 __PPO__SelfCenter() { return STRUCT_OFFSET(USTUNextLocationTask, SelfCenter); } \
	FORCEINLINE static uint32 __PPO__CenterActorKey() { return STRUCT_OFFSET(USTUNextLocationTask, CenterActorKey); }


#define shooter_Source_ShootThemUp_Public_AI_Tasks_STUNextLocationTask_h_9_PROLOG
#define shooter_Source_ShootThemUp_Public_AI_Tasks_STUNextLocationTask_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	shooter_Source_ShootThemUp_Public_AI_Tasks_STUNextLocationTask_h_12_PRIVATE_PROPERTY_OFFSET \
	shooter_Source_ShootThemUp_Public_AI_Tasks_STUNextLocationTask_h_12_SPARSE_DATA \
	shooter_Source_ShootThemUp_Public_AI_Tasks_STUNextLocationTask_h_12_RPC_WRAPPERS \
	shooter_Source_ShootThemUp_Public_AI_Tasks_STUNextLocationTask_h_12_INCLASS \
	shooter_Source_ShootThemUp_Public_AI_Tasks_STUNextLocationTask_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define shooter_Source_ShootThemUp_Public_AI_Tasks_STUNextLocationTask_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	shooter_Source_ShootThemUp_Public_AI_Tasks_STUNextLocationTask_h_12_PRIVATE_PROPERTY_OFFSET \
	shooter_Source_ShootThemUp_Public_AI_Tasks_STUNextLocationTask_h_12_SPARSE_DATA \
	shooter_Source_ShootThemUp_Public_AI_Tasks_STUNextLocationTask_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	shooter_Source_ShootThemUp_Public_AI_Tasks_STUNextLocationTask_h_12_INCLASS_NO_PURE_DECLS \
	shooter_Source_ShootThemUp_Public_AI_Tasks_STUNextLocationTask_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SHOOTTHEMUP_API UClass* StaticClass<class USTUNextLocationTask>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID shooter_Source_ShootThemUp_Public_AI_Tasks_STUNextLocationTask_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
