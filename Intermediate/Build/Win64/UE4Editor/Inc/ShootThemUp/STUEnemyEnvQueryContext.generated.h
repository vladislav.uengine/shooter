// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SHOOTTHEMUP_STUEnemyEnvQueryContext_generated_h
#error "STUEnemyEnvQueryContext.generated.h already included, missing '#pragma once' in STUEnemyEnvQueryContext.h"
#endif
#define SHOOTTHEMUP_STUEnemyEnvQueryContext_generated_h

#define shooter_Source_ShootThemUp_Public_AI_EQS_STUEnemyEnvQueryContext_h_15_SPARSE_DATA
#define shooter_Source_ShootThemUp_Public_AI_EQS_STUEnemyEnvQueryContext_h_15_RPC_WRAPPERS
#define shooter_Source_ShootThemUp_Public_AI_EQS_STUEnemyEnvQueryContext_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define shooter_Source_ShootThemUp_Public_AI_EQS_STUEnemyEnvQueryContext_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSTUEnemyEnvQueryContext(); \
	friend struct Z_Construct_UClass_USTUEnemyEnvQueryContext_Statics; \
public: \
	DECLARE_CLASS(USTUEnemyEnvQueryContext, UEnvQueryContext, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ShootThemUp"), NO_API) \
	DECLARE_SERIALIZER(USTUEnemyEnvQueryContext)


#define shooter_Source_ShootThemUp_Public_AI_EQS_STUEnemyEnvQueryContext_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUSTUEnemyEnvQueryContext(); \
	friend struct Z_Construct_UClass_USTUEnemyEnvQueryContext_Statics; \
public: \
	DECLARE_CLASS(USTUEnemyEnvQueryContext, UEnvQueryContext, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ShootThemUp"), NO_API) \
	DECLARE_SERIALIZER(USTUEnemyEnvQueryContext)


#define shooter_Source_ShootThemUp_Public_AI_EQS_STUEnemyEnvQueryContext_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USTUEnemyEnvQueryContext(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USTUEnemyEnvQueryContext) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USTUEnemyEnvQueryContext); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USTUEnemyEnvQueryContext); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USTUEnemyEnvQueryContext(USTUEnemyEnvQueryContext&&); \
	NO_API USTUEnemyEnvQueryContext(const USTUEnemyEnvQueryContext&); \
public:


#define shooter_Source_ShootThemUp_Public_AI_EQS_STUEnemyEnvQueryContext_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USTUEnemyEnvQueryContext(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USTUEnemyEnvQueryContext(USTUEnemyEnvQueryContext&&); \
	NO_API USTUEnemyEnvQueryContext(const USTUEnemyEnvQueryContext&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USTUEnemyEnvQueryContext); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USTUEnemyEnvQueryContext); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USTUEnemyEnvQueryContext)


#define shooter_Source_ShootThemUp_Public_AI_EQS_STUEnemyEnvQueryContext_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__EnemyActorKeyName() { return STRUCT_OFFSET(USTUEnemyEnvQueryContext, EnemyActorKeyName); }


#define shooter_Source_ShootThemUp_Public_AI_EQS_STUEnemyEnvQueryContext_h_12_PROLOG
#define shooter_Source_ShootThemUp_Public_AI_EQS_STUEnemyEnvQueryContext_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	shooter_Source_ShootThemUp_Public_AI_EQS_STUEnemyEnvQueryContext_h_15_PRIVATE_PROPERTY_OFFSET \
	shooter_Source_ShootThemUp_Public_AI_EQS_STUEnemyEnvQueryContext_h_15_SPARSE_DATA \
	shooter_Source_ShootThemUp_Public_AI_EQS_STUEnemyEnvQueryContext_h_15_RPC_WRAPPERS \
	shooter_Source_ShootThemUp_Public_AI_EQS_STUEnemyEnvQueryContext_h_15_INCLASS \
	shooter_Source_ShootThemUp_Public_AI_EQS_STUEnemyEnvQueryContext_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define shooter_Source_ShootThemUp_Public_AI_EQS_STUEnemyEnvQueryContext_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	shooter_Source_ShootThemUp_Public_AI_EQS_STUEnemyEnvQueryContext_h_15_PRIVATE_PROPERTY_OFFSET \
	shooter_Source_ShootThemUp_Public_AI_EQS_STUEnemyEnvQueryContext_h_15_SPARSE_DATA \
	shooter_Source_ShootThemUp_Public_AI_EQS_STUEnemyEnvQueryContext_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	shooter_Source_ShootThemUp_Public_AI_EQS_STUEnemyEnvQueryContext_h_15_INCLASS_NO_PURE_DECLS \
	shooter_Source_ShootThemUp_Public_AI_EQS_STUEnemyEnvQueryContext_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SHOOTTHEMUP_API UClass* StaticClass<class USTUEnemyEnvQueryContext>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID shooter_Source_ShootThemUp_Public_AI_EQS_STUEnemyEnvQueryContext_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
