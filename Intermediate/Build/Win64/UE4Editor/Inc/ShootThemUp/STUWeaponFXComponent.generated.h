// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SHOOTTHEMUP_STUWeaponFXComponent_generated_h
#error "STUWeaponFXComponent.generated.h already included, missing '#pragma once' in STUWeaponFXComponent.h"
#endif
#define SHOOTTHEMUP_STUWeaponFXComponent_generated_h

#define shooter_Source_ShootThemUp_Public_Weapon_Components_STUWeaponFXComponent_h_17_SPARSE_DATA
#define shooter_Source_ShootThemUp_Public_Weapon_Components_STUWeaponFXComponent_h_17_RPC_WRAPPERS
#define shooter_Source_ShootThemUp_Public_Weapon_Components_STUWeaponFXComponent_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define shooter_Source_ShootThemUp_Public_Weapon_Components_STUWeaponFXComponent_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSTUWeaponFXComponent(); \
	friend struct Z_Construct_UClass_USTUWeaponFXComponent_Statics; \
public: \
	DECLARE_CLASS(USTUWeaponFXComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ShootThemUp"), NO_API) \
	DECLARE_SERIALIZER(USTUWeaponFXComponent)


#define shooter_Source_ShootThemUp_Public_Weapon_Components_STUWeaponFXComponent_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUSTUWeaponFXComponent(); \
	friend struct Z_Construct_UClass_USTUWeaponFXComponent_Statics; \
public: \
	DECLARE_CLASS(USTUWeaponFXComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ShootThemUp"), NO_API) \
	DECLARE_SERIALIZER(USTUWeaponFXComponent)


#define shooter_Source_ShootThemUp_Public_Weapon_Components_STUWeaponFXComponent_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USTUWeaponFXComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USTUWeaponFXComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USTUWeaponFXComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USTUWeaponFXComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USTUWeaponFXComponent(USTUWeaponFXComponent&&); \
	NO_API USTUWeaponFXComponent(const USTUWeaponFXComponent&); \
public:


#define shooter_Source_ShootThemUp_Public_Weapon_Components_STUWeaponFXComponent_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USTUWeaponFXComponent(USTUWeaponFXComponent&&); \
	NO_API USTUWeaponFXComponent(const USTUWeaponFXComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USTUWeaponFXComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USTUWeaponFXComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(USTUWeaponFXComponent)


#define shooter_Source_ShootThemUp_Public_Weapon_Components_STUWeaponFXComponent_h_17_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__DefaultImpactData() { return STRUCT_OFFSET(USTUWeaponFXComponent, DefaultImpactData); } \
	FORCEINLINE static uint32 __PPO__ImpactDataMap() { return STRUCT_OFFSET(USTUWeaponFXComponent, ImpactDataMap); }


#define shooter_Source_ShootThemUp_Public_Weapon_Components_STUWeaponFXComponent_h_14_PROLOG
#define shooter_Source_ShootThemUp_Public_Weapon_Components_STUWeaponFXComponent_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	shooter_Source_ShootThemUp_Public_Weapon_Components_STUWeaponFXComponent_h_17_PRIVATE_PROPERTY_OFFSET \
	shooter_Source_ShootThemUp_Public_Weapon_Components_STUWeaponFXComponent_h_17_SPARSE_DATA \
	shooter_Source_ShootThemUp_Public_Weapon_Components_STUWeaponFXComponent_h_17_RPC_WRAPPERS \
	shooter_Source_ShootThemUp_Public_Weapon_Components_STUWeaponFXComponent_h_17_INCLASS \
	shooter_Source_ShootThemUp_Public_Weapon_Components_STUWeaponFXComponent_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define shooter_Source_ShootThemUp_Public_Weapon_Components_STUWeaponFXComponent_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	shooter_Source_ShootThemUp_Public_Weapon_Components_STUWeaponFXComponent_h_17_PRIVATE_PROPERTY_OFFSET \
	shooter_Source_ShootThemUp_Public_Weapon_Components_STUWeaponFXComponent_h_17_SPARSE_DATA \
	shooter_Source_ShootThemUp_Public_Weapon_Components_STUWeaponFXComponent_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	shooter_Source_ShootThemUp_Public_Weapon_Components_STUWeaponFXComponent_h_17_INCLASS_NO_PURE_DECLS \
	shooter_Source_ShootThemUp_Public_Weapon_Components_STUWeaponFXComponent_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SHOOTTHEMUP_API UClass* StaticClass<class USTUWeaponFXComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID shooter_Source_ShootThemUp_Public_Weapon_Components_STUWeaponFXComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
