// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SHOOTTHEMUP_STUAIPerceptionComponent_generated_h
#error "STUAIPerceptionComponent.generated.h already included, missing '#pragma once' in STUAIPerceptionComponent.h"
#endif
#define SHOOTTHEMUP_STUAIPerceptionComponent_generated_h

#define shooter_Source_ShootThemUp_Public_Components_STUAIPerceptionComponent_h_15_SPARSE_DATA
#define shooter_Source_ShootThemUp_Public_Components_STUAIPerceptionComponent_h_15_RPC_WRAPPERS
#define shooter_Source_ShootThemUp_Public_Components_STUAIPerceptionComponent_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define shooter_Source_ShootThemUp_Public_Components_STUAIPerceptionComponent_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSTUAIPerceptionComponent(); \
	friend struct Z_Construct_UClass_USTUAIPerceptionComponent_Statics; \
public: \
	DECLARE_CLASS(USTUAIPerceptionComponent, UAIPerceptionComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ShootThemUp"), NO_API) \
	DECLARE_SERIALIZER(USTUAIPerceptionComponent)


#define shooter_Source_ShootThemUp_Public_Components_STUAIPerceptionComponent_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUSTUAIPerceptionComponent(); \
	friend struct Z_Construct_UClass_USTUAIPerceptionComponent_Statics; \
public: \
	DECLARE_CLASS(USTUAIPerceptionComponent, UAIPerceptionComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ShootThemUp"), NO_API) \
	DECLARE_SERIALIZER(USTUAIPerceptionComponent)


#define shooter_Source_ShootThemUp_Public_Components_STUAIPerceptionComponent_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USTUAIPerceptionComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USTUAIPerceptionComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USTUAIPerceptionComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USTUAIPerceptionComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USTUAIPerceptionComponent(USTUAIPerceptionComponent&&); \
	NO_API USTUAIPerceptionComponent(const USTUAIPerceptionComponent&); \
public:


#define shooter_Source_ShootThemUp_Public_Components_STUAIPerceptionComponent_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USTUAIPerceptionComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USTUAIPerceptionComponent(USTUAIPerceptionComponent&&); \
	NO_API USTUAIPerceptionComponent(const USTUAIPerceptionComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USTUAIPerceptionComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USTUAIPerceptionComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USTUAIPerceptionComponent)


#define shooter_Source_ShootThemUp_Public_Components_STUAIPerceptionComponent_h_15_PRIVATE_PROPERTY_OFFSET
#define shooter_Source_ShootThemUp_Public_Components_STUAIPerceptionComponent_h_12_PROLOG
#define shooter_Source_ShootThemUp_Public_Components_STUAIPerceptionComponent_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	shooter_Source_ShootThemUp_Public_Components_STUAIPerceptionComponent_h_15_PRIVATE_PROPERTY_OFFSET \
	shooter_Source_ShootThemUp_Public_Components_STUAIPerceptionComponent_h_15_SPARSE_DATA \
	shooter_Source_ShootThemUp_Public_Components_STUAIPerceptionComponent_h_15_RPC_WRAPPERS \
	shooter_Source_ShootThemUp_Public_Components_STUAIPerceptionComponent_h_15_INCLASS \
	shooter_Source_ShootThemUp_Public_Components_STUAIPerceptionComponent_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define shooter_Source_ShootThemUp_Public_Components_STUAIPerceptionComponent_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	shooter_Source_ShootThemUp_Public_Components_STUAIPerceptionComponent_h_15_PRIVATE_PROPERTY_OFFSET \
	shooter_Source_ShootThemUp_Public_Components_STUAIPerceptionComponent_h_15_SPARSE_DATA \
	shooter_Source_ShootThemUp_Public_Components_STUAIPerceptionComponent_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	shooter_Source_ShootThemUp_Public_Components_STUAIPerceptionComponent_h_15_INCLASS_NO_PURE_DECLS \
	shooter_Source_ShootThemUp_Public_Components_STUAIPerceptionComponent_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SHOOTTHEMUP_API UClass* StaticClass<class USTUAIPerceptionComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID shooter_Source_ShootThemUp_Public_Components_STUAIPerceptionComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
