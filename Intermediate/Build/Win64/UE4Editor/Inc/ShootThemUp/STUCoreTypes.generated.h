// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SHOOTTHEMUP_STUCoreTypes_generated_h
#error "STUCoreTypes.generated.h already included, missing '#pragma once' in STUCoreTypes.h"
#endif
#define SHOOTTHEMUP_STUCoreTypes_generated_h

#define shooter_Source_ShootThemUp_Public_STUCoreTypes_h_127_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FLevelData_Statics; \
	SHOOTTHEMUP_API static class UScriptStruct* StaticStruct();


template<> SHOOTTHEMUP_API UScriptStruct* StaticStruct<struct FLevelData>();

#define shooter_Source_ShootThemUp_Public_STUCoreTypes_h_92_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FGameData_Statics; \
	SHOOTTHEMUP_API static class UScriptStruct* StaticStruct();


template<> SHOOTTHEMUP_API UScriptStruct* StaticStruct<struct FGameData>();

#define shooter_Source_ShootThemUp_Public_STUCoreTypes_h_77_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FImpactData_Statics; \
	SHOOTTHEMUP_API static class UScriptStruct* StaticStruct();


template<> SHOOTTHEMUP_API UScriptStruct* StaticStruct<struct FImpactData>();

#define shooter_Source_ShootThemUp_Public_STUCoreTypes_h_59_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDecalData_Statics; \
	SHOOTTHEMUP_API static class UScriptStruct* StaticStruct();


template<> SHOOTTHEMUP_API UScriptStruct* StaticStruct<struct FDecalData>();

#define shooter_Source_ShootThemUp_Public_STUCoreTypes_h_39_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FWeaponUIData_Statics; \
	SHOOTTHEMUP_API static class UScriptStruct* StaticStruct();


template<> SHOOTTHEMUP_API UScriptStruct* StaticStruct<struct FWeaponUIData>();

#define shooter_Source_ShootThemUp_Public_STUCoreTypes_h_27_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FWeaponData_Statics; \
	SHOOTTHEMUP_API static class UScriptStruct* StaticStruct();


template<> SHOOTTHEMUP_API UScriptStruct* StaticStruct<struct FWeaponData>();

#define shooter_Source_ShootThemUp_Public_STUCoreTypes_h_12_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FAmmoData_Statics; \
	SHOOTTHEMUP_API static class UScriptStruct* StaticStruct();


template<> SHOOTTHEMUP_API UScriptStruct* StaticStruct<struct FAmmoData>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID shooter_Source_ShootThemUp_Public_STUCoreTypes_h


#define FOREACH_ENUM_ESTUMATCHSTATE(op) \
	op(ESTUMatchState::WaitingToStart) \
	op(ESTUMatchState::InProgress) \
	op(ESTUMatchState::Pause) \
	op(ESTUMatchState::GameOver) 

enum class ESTUMatchState : uint8;
template<> SHOOTTHEMUP_API UEnum* StaticEnum<ESTUMatchState>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
