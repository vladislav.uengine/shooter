// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SHOOTTHEMUP_STUPlayerStatRowWidget_generated_h
#error "STUPlayerStatRowWidget.generated.h already included, missing '#pragma once' in STUPlayerStatRowWidget.h"
#endif
#define SHOOTTHEMUP_STUPlayerStatRowWidget_generated_h

#define shooter_Source_ShootThemUp_Public_UI_STUPlayerStatRowWidget_h_15_SPARSE_DATA
#define shooter_Source_ShootThemUp_Public_UI_STUPlayerStatRowWidget_h_15_RPC_WRAPPERS
#define shooter_Source_ShootThemUp_Public_UI_STUPlayerStatRowWidget_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define shooter_Source_ShootThemUp_Public_UI_STUPlayerStatRowWidget_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSTUPlayerStatRowWidget(); \
	friend struct Z_Construct_UClass_USTUPlayerStatRowWidget_Statics; \
public: \
	DECLARE_CLASS(USTUPlayerStatRowWidget, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ShootThemUp"), NO_API) \
	DECLARE_SERIALIZER(USTUPlayerStatRowWidget)


#define shooter_Source_ShootThemUp_Public_UI_STUPlayerStatRowWidget_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUSTUPlayerStatRowWidget(); \
	friend struct Z_Construct_UClass_USTUPlayerStatRowWidget_Statics; \
public: \
	DECLARE_CLASS(USTUPlayerStatRowWidget, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ShootThemUp"), NO_API) \
	DECLARE_SERIALIZER(USTUPlayerStatRowWidget)


#define shooter_Source_ShootThemUp_Public_UI_STUPlayerStatRowWidget_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USTUPlayerStatRowWidget(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USTUPlayerStatRowWidget) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USTUPlayerStatRowWidget); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USTUPlayerStatRowWidget); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USTUPlayerStatRowWidget(USTUPlayerStatRowWidget&&); \
	NO_API USTUPlayerStatRowWidget(const USTUPlayerStatRowWidget&); \
public:


#define shooter_Source_ShootThemUp_Public_UI_STUPlayerStatRowWidget_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USTUPlayerStatRowWidget(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USTUPlayerStatRowWidget(USTUPlayerStatRowWidget&&); \
	NO_API USTUPlayerStatRowWidget(const USTUPlayerStatRowWidget&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USTUPlayerStatRowWidget); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USTUPlayerStatRowWidget); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USTUPlayerStatRowWidget)


#define shooter_Source_ShootThemUp_Public_UI_STUPlayerStatRowWidget_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__PlayerNameTextBlock() { return STRUCT_OFFSET(USTUPlayerStatRowWidget, PlayerNameTextBlock); } \
	FORCEINLINE static uint32 __PPO__KillsTextBlock() { return STRUCT_OFFSET(USTUPlayerStatRowWidget, KillsTextBlock); } \
	FORCEINLINE static uint32 __PPO__DeathsTextBlock() { return STRUCT_OFFSET(USTUPlayerStatRowWidget, DeathsTextBlock); } \
	FORCEINLINE static uint32 __PPO__TeamTextBlock() { return STRUCT_OFFSET(USTUPlayerStatRowWidget, TeamTextBlock); } \
	FORCEINLINE static uint32 __PPO__PlayerIndicatorImage() { return STRUCT_OFFSET(USTUPlayerStatRowWidget, PlayerIndicatorImage); } \
	FORCEINLINE static uint32 __PPO__TeamImage() { return STRUCT_OFFSET(USTUPlayerStatRowWidget, TeamImage); }


#define shooter_Source_ShootThemUp_Public_UI_STUPlayerStatRowWidget_h_12_PROLOG
#define shooter_Source_ShootThemUp_Public_UI_STUPlayerStatRowWidget_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	shooter_Source_ShootThemUp_Public_UI_STUPlayerStatRowWidget_h_15_PRIVATE_PROPERTY_OFFSET \
	shooter_Source_ShootThemUp_Public_UI_STUPlayerStatRowWidget_h_15_SPARSE_DATA \
	shooter_Source_ShootThemUp_Public_UI_STUPlayerStatRowWidget_h_15_RPC_WRAPPERS \
	shooter_Source_ShootThemUp_Public_UI_STUPlayerStatRowWidget_h_15_INCLASS \
	shooter_Source_ShootThemUp_Public_UI_STUPlayerStatRowWidget_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define shooter_Source_ShootThemUp_Public_UI_STUPlayerStatRowWidget_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	shooter_Source_ShootThemUp_Public_UI_STUPlayerStatRowWidget_h_15_PRIVATE_PROPERTY_OFFSET \
	shooter_Source_ShootThemUp_Public_UI_STUPlayerStatRowWidget_h_15_SPARSE_DATA \
	shooter_Source_ShootThemUp_Public_UI_STUPlayerStatRowWidget_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	shooter_Source_ShootThemUp_Public_UI_STUPlayerStatRowWidget_h_15_INCLASS_NO_PURE_DECLS \
	shooter_Source_ShootThemUp_Public_UI_STUPlayerStatRowWidget_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SHOOTTHEMUP_API UClass* StaticClass<class USTUPlayerStatRowWidget>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID shooter_Source_ShootThemUp_Public_UI_STUPlayerStatRowWidget_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
