// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SHOOTTHEMUP_STURespawnComponent_generated_h
#error "STURespawnComponent.generated.h already included, missing '#pragma once' in STURespawnComponent.h"
#endif
#define SHOOTTHEMUP_STURespawnComponent_generated_h

#define shooter_Source_ShootThemUp_Public_Components_STURespawnComponent_h_13_SPARSE_DATA
#define shooter_Source_ShootThemUp_Public_Components_STURespawnComponent_h_13_RPC_WRAPPERS
#define shooter_Source_ShootThemUp_Public_Components_STURespawnComponent_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define shooter_Source_ShootThemUp_Public_Components_STURespawnComponent_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSTURespawnComponent(); \
	friend struct Z_Construct_UClass_USTURespawnComponent_Statics; \
public: \
	DECLARE_CLASS(USTURespawnComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ShootThemUp"), NO_API) \
	DECLARE_SERIALIZER(USTURespawnComponent)


#define shooter_Source_ShootThemUp_Public_Components_STURespawnComponent_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUSTURespawnComponent(); \
	friend struct Z_Construct_UClass_USTURespawnComponent_Statics; \
public: \
	DECLARE_CLASS(USTURespawnComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ShootThemUp"), NO_API) \
	DECLARE_SERIALIZER(USTURespawnComponent)


#define shooter_Source_ShootThemUp_Public_Components_STURespawnComponent_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USTURespawnComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USTURespawnComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USTURespawnComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USTURespawnComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USTURespawnComponent(USTURespawnComponent&&); \
	NO_API USTURespawnComponent(const USTURespawnComponent&); \
public:


#define shooter_Source_ShootThemUp_Public_Components_STURespawnComponent_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USTURespawnComponent(USTURespawnComponent&&); \
	NO_API USTURespawnComponent(const USTURespawnComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USTURespawnComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USTURespawnComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(USTURespawnComponent)


#define shooter_Source_ShootThemUp_Public_Components_STURespawnComponent_h_13_PRIVATE_PROPERTY_OFFSET
#define shooter_Source_ShootThemUp_Public_Components_STURespawnComponent_h_10_PROLOG
#define shooter_Source_ShootThemUp_Public_Components_STURespawnComponent_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	shooter_Source_ShootThemUp_Public_Components_STURespawnComponent_h_13_PRIVATE_PROPERTY_OFFSET \
	shooter_Source_ShootThemUp_Public_Components_STURespawnComponent_h_13_SPARSE_DATA \
	shooter_Source_ShootThemUp_Public_Components_STURespawnComponent_h_13_RPC_WRAPPERS \
	shooter_Source_ShootThemUp_Public_Components_STURespawnComponent_h_13_INCLASS \
	shooter_Source_ShootThemUp_Public_Components_STURespawnComponent_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define shooter_Source_ShootThemUp_Public_Components_STURespawnComponent_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	shooter_Source_ShootThemUp_Public_Components_STURespawnComponent_h_13_PRIVATE_PROPERTY_OFFSET \
	shooter_Source_ShootThemUp_Public_Components_STURespawnComponent_h_13_SPARSE_DATA \
	shooter_Source_ShootThemUp_Public_Components_STURespawnComponent_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	shooter_Source_ShootThemUp_Public_Components_STURespawnComponent_h_13_INCLASS_NO_PURE_DECLS \
	shooter_Source_ShootThemUp_Public_Components_STURespawnComponent_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SHOOTTHEMUP_API UClass* StaticClass<class USTURespawnComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID shooter_Source_ShootThemUp_Public_Components_STURespawnComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
