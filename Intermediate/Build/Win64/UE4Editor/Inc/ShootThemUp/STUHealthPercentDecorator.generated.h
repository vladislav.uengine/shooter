// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SHOOTTHEMUP_STUHealthPercentDecorator_generated_h
#error "STUHealthPercentDecorator.generated.h already included, missing '#pragma once' in STUHealthPercentDecorator.h"
#endif
#define SHOOTTHEMUP_STUHealthPercentDecorator_generated_h

#define shooter_Source_ShootThemUp_Public_AI_Decorators_STUHealthPercentDecorator_h_15_SPARSE_DATA
#define shooter_Source_ShootThemUp_Public_AI_Decorators_STUHealthPercentDecorator_h_15_RPC_WRAPPERS
#define shooter_Source_ShootThemUp_Public_AI_Decorators_STUHealthPercentDecorator_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define shooter_Source_ShootThemUp_Public_AI_Decorators_STUHealthPercentDecorator_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSTUHealthPercentDecorator(); \
	friend struct Z_Construct_UClass_USTUHealthPercentDecorator_Statics; \
public: \
	DECLARE_CLASS(USTUHealthPercentDecorator, UBTDecorator, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ShootThemUp"), NO_API) \
	DECLARE_SERIALIZER(USTUHealthPercentDecorator)


#define shooter_Source_ShootThemUp_Public_AI_Decorators_STUHealthPercentDecorator_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUSTUHealthPercentDecorator(); \
	friend struct Z_Construct_UClass_USTUHealthPercentDecorator_Statics; \
public: \
	DECLARE_CLASS(USTUHealthPercentDecorator, UBTDecorator, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ShootThemUp"), NO_API) \
	DECLARE_SERIALIZER(USTUHealthPercentDecorator)


#define shooter_Source_ShootThemUp_Public_AI_Decorators_STUHealthPercentDecorator_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USTUHealthPercentDecorator(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USTUHealthPercentDecorator) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USTUHealthPercentDecorator); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USTUHealthPercentDecorator); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USTUHealthPercentDecorator(USTUHealthPercentDecorator&&); \
	NO_API USTUHealthPercentDecorator(const USTUHealthPercentDecorator&); \
public:


#define shooter_Source_ShootThemUp_Public_AI_Decorators_STUHealthPercentDecorator_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USTUHealthPercentDecorator(USTUHealthPercentDecorator&&); \
	NO_API USTUHealthPercentDecorator(const USTUHealthPercentDecorator&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USTUHealthPercentDecorator); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USTUHealthPercentDecorator); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(USTUHealthPercentDecorator)


#define shooter_Source_ShootThemUp_Public_AI_Decorators_STUHealthPercentDecorator_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__HealthPercent() { return STRUCT_OFFSET(USTUHealthPercentDecorator, HealthPercent); }


#define shooter_Source_ShootThemUp_Public_AI_Decorators_STUHealthPercentDecorator_h_12_PROLOG
#define shooter_Source_ShootThemUp_Public_AI_Decorators_STUHealthPercentDecorator_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	shooter_Source_ShootThemUp_Public_AI_Decorators_STUHealthPercentDecorator_h_15_PRIVATE_PROPERTY_OFFSET \
	shooter_Source_ShootThemUp_Public_AI_Decorators_STUHealthPercentDecorator_h_15_SPARSE_DATA \
	shooter_Source_ShootThemUp_Public_AI_Decorators_STUHealthPercentDecorator_h_15_RPC_WRAPPERS \
	shooter_Source_ShootThemUp_Public_AI_Decorators_STUHealthPercentDecorator_h_15_INCLASS \
	shooter_Source_ShootThemUp_Public_AI_Decorators_STUHealthPercentDecorator_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define shooter_Source_ShootThemUp_Public_AI_Decorators_STUHealthPercentDecorator_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	shooter_Source_ShootThemUp_Public_AI_Decorators_STUHealthPercentDecorator_h_15_PRIVATE_PROPERTY_OFFSET \
	shooter_Source_ShootThemUp_Public_AI_Decorators_STUHealthPercentDecorator_h_15_SPARSE_DATA \
	shooter_Source_ShootThemUp_Public_AI_Decorators_STUHealthPercentDecorator_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	shooter_Source_ShootThemUp_Public_AI_Decorators_STUHealthPercentDecorator_h_15_INCLASS_NO_PURE_DECLS \
	shooter_Source_ShootThemUp_Public_AI_Decorators_STUHealthPercentDecorator_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SHOOTTHEMUP_API UClass* StaticClass<class USTUHealthPercentDecorator>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID shooter_Source_ShootThemUp_Public_AI_Decorators_STUHealthPercentDecorator_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
