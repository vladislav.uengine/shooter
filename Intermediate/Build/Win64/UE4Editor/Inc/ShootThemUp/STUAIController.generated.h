// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SHOOTTHEMUP_STUAIController_generated_h
#error "STUAIController.generated.h already included, missing '#pragma once' in STUAIController.h"
#endif
#define SHOOTTHEMUP_STUAIController_generated_h

#define shooter_Source_ShootThemUp_Public_AI_STUAIController_h_15_SPARSE_DATA
#define shooter_Source_ShootThemUp_Public_AI_STUAIController_h_15_RPC_WRAPPERS
#define shooter_Source_ShootThemUp_Public_AI_STUAIController_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define shooter_Source_ShootThemUp_Public_AI_STUAIController_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASTUAIController(); \
	friend struct Z_Construct_UClass_ASTUAIController_Statics; \
public: \
	DECLARE_CLASS(ASTUAIController, AAIController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ShootThemUp"), NO_API) \
	DECLARE_SERIALIZER(ASTUAIController)


#define shooter_Source_ShootThemUp_Public_AI_STUAIController_h_15_INCLASS \
private: \
	static void StaticRegisterNativesASTUAIController(); \
	friend struct Z_Construct_UClass_ASTUAIController_Statics; \
public: \
	DECLARE_CLASS(ASTUAIController, AAIController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ShootThemUp"), NO_API) \
	DECLARE_SERIALIZER(ASTUAIController)


#define shooter_Source_ShootThemUp_Public_AI_STUAIController_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASTUAIController(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASTUAIController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASTUAIController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASTUAIController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASTUAIController(ASTUAIController&&); \
	NO_API ASTUAIController(const ASTUAIController&); \
public:


#define shooter_Source_ShootThemUp_Public_AI_STUAIController_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASTUAIController(ASTUAIController&&); \
	NO_API ASTUAIController(const ASTUAIController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASTUAIController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASTUAIController); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASTUAIController)


#define shooter_Source_ShootThemUp_Public_AI_STUAIController_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__STUAIPerceptionComponent() { return STRUCT_OFFSET(ASTUAIController, STUAIPerceptionComponent); } \
	FORCEINLINE static uint32 __PPO__RespawnComponent() { return STRUCT_OFFSET(ASTUAIController, RespawnComponent); } \
	FORCEINLINE static uint32 __PPO__FocusOnKeyName() { return STRUCT_OFFSET(ASTUAIController, FocusOnKeyName); }


#define shooter_Source_ShootThemUp_Public_AI_STUAIController_h_12_PROLOG
#define shooter_Source_ShootThemUp_Public_AI_STUAIController_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	shooter_Source_ShootThemUp_Public_AI_STUAIController_h_15_PRIVATE_PROPERTY_OFFSET \
	shooter_Source_ShootThemUp_Public_AI_STUAIController_h_15_SPARSE_DATA \
	shooter_Source_ShootThemUp_Public_AI_STUAIController_h_15_RPC_WRAPPERS \
	shooter_Source_ShootThemUp_Public_AI_STUAIController_h_15_INCLASS \
	shooter_Source_ShootThemUp_Public_AI_STUAIController_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define shooter_Source_ShootThemUp_Public_AI_STUAIController_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	shooter_Source_ShootThemUp_Public_AI_STUAIController_h_15_PRIVATE_PROPERTY_OFFSET \
	shooter_Source_ShootThemUp_Public_AI_STUAIController_h_15_SPARSE_DATA \
	shooter_Source_ShootThemUp_Public_AI_STUAIController_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	shooter_Source_ShootThemUp_Public_AI_STUAIController_h_15_INCLASS_NO_PURE_DECLS \
	shooter_Source_ShootThemUp_Public_AI_STUAIController_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SHOOTTHEMUP_API UClass* StaticClass<class ASTUAIController>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID shooter_Source_ShootThemUp_Public_AI_STUAIController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
