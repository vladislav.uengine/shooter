// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class USoundClass;
#ifdef SHOOTTHEMUP_STUSoundFuncLib_generated_h
#error "STUSoundFuncLib.generated.h already included, missing '#pragma once' in STUSoundFuncLib.h"
#endif
#define SHOOTTHEMUP_STUSoundFuncLib_generated_h

#define shooter_Source_ShootThemUp_Public_Sound_STUSoundFuncLib_h_14_SPARSE_DATA
#define shooter_Source_ShootThemUp_Public_Sound_STUSoundFuncLib_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execToggleSoundClassVolume); \
	DECLARE_FUNCTION(execSetSoundClassVolume);


#define shooter_Source_ShootThemUp_Public_Sound_STUSoundFuncLib_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execToggleSoundClassVolume); \
	DECLARE_FUNCTION(execSetSoundClassVolume);


#define shooter_Source_ShootThemUp_Public_Sound_STUSoundFuncLib_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSTUSoundFuncLib(); \
	friend struct Z_Construct_UClass_USTUSoundFuncLib_Statics; \
public: \
	DECLARE_CLASS(USTUSoundFuncLib, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ShootThemUp"), NO_API) \
	DECLARE_SERIALIZER(USTUSoundFuncLib)


#define shooter_Source_ShootThemUp_Public_Sound_STUSoundFuncLib_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUSTUSoundFuncLib(); \
	friend struct Z_Construct_UClass_USTUSoundFuncLib_Statics; \
public: \
	DECLARE_CLASS(USTUSoundFuncLib, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ShootThemUp"), NO_API) \
	DECLARE_SERIALIZER(USTUSoundFuncLib)


#define shooter_Source_ShootThemUp_Public_Sound_STUSoundFuncLib_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USTUSoundFuncLib(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USTUSoundFuncLib) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USTUSoundFuncLib); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USTUSoundFuncLib); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USTUSoundFuncLib(USTUSoundFuncLib&&); \
	NO_API USTUSoundFuncLib(const USTUSoundFuncLib&); \
public:


#define shooter_Source_ShootThemUp_Public_Sound_STUSoundFuncLib_h_14_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USTUSoundFuncLib(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USTUSoundFuncLib(USTUSoundFuncLib&&); \
	NO_API USTUSoundFuncLib(const USTUSoundFuncLib&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USTUSoundFuncLib); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USTUSoundFuncLib); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USTUSoundFuncLib)


#define shooter_Source_ShootThemUp_Public_Sound_STUSoundFuncLib_h_14_PRIVATE_PROPERTY_OFFSET
#define shooter_Source_ShootThemUp_Public_Sound_STUSoundFuncLib_h_11_PROLOG
#define shooter_Source_ShootThemUp_Public_Sound_STUSoundFuncLib_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	shooter_Source_ShootThemUp_Public_Sound_STUSoundFuncLib_h_14_PRIVATE_PROPERTY_OFFSET \
	shooter_Source_ShootThemUp_Public_Sound_STUSoundFuncLib_h_14_SPARSE_DATA \
	shooter_Source_ShootThemUp_Public_Sound_STUSoundFuncLib_h_14_RPC_WRAPPERS \
	shooter_Source_ShootThemUp_Public_Sound_STUSoundFuncLib_h_14_INCLASS \
	shooter_Source_ShootThemUp_Public_Sound_STUSoundFuncLib_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define shooter_Source_ShootThemUp_Public_Sound_STUSoundFuncLib_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	shooter_Source_ShootThemUp_Public_Sound_STUSoundFuncLib_h_14_PRIVATE_PROPERTY_OFFSET \
	shooter_Source_ShootThemUp_Public_Sound_STUSoundFuncLib_h_14_SPARSE_DATA \
	shooter_Source_ShootThemUp_Public_Sound_STUSoundFuncLib_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	shooter_Source_ShootThemUp_Public_Sound_STUSoundFuncLib_h_14_INCLASS_NO_PURE_DECLS \
	shooter_Source_ShootThemUp_Public_Sound_STUSoundFuncLib_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SHOOTTHEMUP_API UClass* StaticClass<class USTUSoundFuncLib>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID shooter_Source_ShootThemUp_Public_Sound_STUSoundFuncLib_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
