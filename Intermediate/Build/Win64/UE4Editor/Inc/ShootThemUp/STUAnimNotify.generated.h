// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SHOOTTHEMUP_STUAnimNotify_generated_h
#error "STUAnimNotify.generated.h already included, missing '#pragma once' in STUAnimNotify.h"
#endif
#define SHOOTTHEMUP_STUAnimNotify_generated_h

#define shooter_Source_ShootThemUp_Public_Animations_STUAnimNotify_h_14_SPARSE_DATA
#define shooter_Source_ShootThemUp_Public_Animations_STUAnimNotify_h_14_RPC_WRAPPERS
#define shooter_Source_ShootThemUp_Public_Animations_STUAnimNotify_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define shooter_Source_ShootThemUp_Public_Animations_STUAnimNotify_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSTUAnimNotify(); \
	friend struct Z_Construct_UClass_USTUAnimNotify_Statics; \
public: \
	DECLARE_CLASS(USTUAnimNotify, UAnimNotify, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ShootThemUp"), NO_API) \
	DECLARE_SERIALIZER(USTUAnimNotify)


#define shooter_Source_ShootThemUp_Public_Animations_STUAnimNotify_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUSTUAnimNotify(); \
	friend struct Z_Construct_UClass_USTUAnimNotify_Statics; \
public: \
	DECLARE_CLASS(USTUAnimNotify, UAnimNotify, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ShootThemUp"), NO_API) \
	DECLARE_SERIALIZER(USTUAnimNotify)


#define shooter_Source_ShootThemUp_Public_Animations_STUAnimNotify_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USTUAnimNotify(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USTUAnimNotify) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USTUAnimNotify); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USTUAnimNotify); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USTUAnimNotify(USTUAnimNotify&&); \
	NO_API USTUAnimNotify(const USTUAnimNotify&); \
public:


#define shooter_Source_ShootThemUp_Public_Animations_STUAnimNotify_h_14_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USTUAnimNotify(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USTUAnimNotify(USTUAnimNotify&&); \
	NO_API USTUAnimNotify(const USTUAnimNotify&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USTUAnimNotify); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USTUAnimNotify); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USTUAnimNotify)


#define shooter_Source_ShootThemUp_Public_Animations_STUAnimNotify_h_14_PRIVATE_PROPERTY_OFFSET
#define shooter_Source_ShootThemUp_Public_Animations_STUAnimNotify_h_11_PROLOG
#define shooter_Source_ShootThemUp_Public_Animations_STUAnimNotify_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	shooter_Source_ShootThemUp_Public_Animations_STUAnimNotify_h_14_PRIVATE_PROPERTY_OFFSET \
	shooter_Source_ShootThemUp_Public_Animations_STUAnimNotify_h_14_SPARSE_DATA \
	shooter_Source_ShootThemUp_Public_Animations_STUAnimNotify_h_14_RPC_WRAPPERS \
	shooter_Source_ShootThemUp_Public_Animations_STUAnimNotify_h_14_INCLASS \
	shooter_Source_ShootThemUp_Public_Animations_STUAnimNotify_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define shooter_Source_ShootThemUp_Public_Animations_STUAnimNotify_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	shooter_Source_ShootThemUp_Public_Animations_STUAnimNotify_h_14_PRIVATE_PROPERTY_OFFSET \
	shooter_Source_ShootThemUp_Public_Animations_STUAnimNotify_h_14_SPARSE_DATA \
	shooter_Source_ShootThemUp_Public_Animations_STUAnimNotify_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	shooter_Source_ShootThemUp_Public_Animations_STUAnimNotify_h_14_INCLASS_NO_PURE_DECLS \
	shooter_Source_ShootThemUp_Public_Animations_STUAnimNotify_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SHOOTTHEMUP_API UClass* StaticClass<class USTUAnimNotify>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID shooter_Source_ShootThemUp_Public_Animations_STUAnimNotify_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
