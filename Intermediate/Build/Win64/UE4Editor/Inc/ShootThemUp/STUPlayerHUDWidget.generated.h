// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FAmmoData;
struct FWeaponUIData;
#ifdef SHOOTTHEMUP_STUPlayerHUDWidget_generated_h
#error "STUPlayerHUDWidget.generated.h already included, missing '#pragma once' in STUPlayerHUDWidget.h"
#endif
#define SHOOTTHEMUP_STUPlayerHUDWidget_generated_h

#define shooter_Source_ShootThemUp_Public_UI_STUPlayerHUDWidget_h_15_SPARSE_DATA
#define shooter_Source_ShootThemUp_Public_UI_STUPlayerHUDWidget_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetKillsNum); \
	DECLARE_FUNCTION(execIsPlayerSpectating); \
	DECLARE_FUNCTION(execIsPlayerAlive); \
	DECLARE_FUNCTION(execGetCurrentWeaponAmmoData); \
	DECLARE_FUNCTION(execGetCurrentWeaponUIData); \
	DECLARE_FUNCTION(execGetHealthPercent);


#define shooter_Source_ShootThemUp_Public_UI_STUPlayerHUDWidget_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetKillsNum); \
	DECLARE_FUNCTION(execIsPlayerSpectating); \
	DECLARE_FUNCTION(execIsPlayerAlive); \
	DECLARE_FUNCTION(execGetCurrentWeaponAmmoData); \
	DECLARE_FUNCTION(execGetCurrentWeaponUIData); \
	DECLARE_FUNCTION(execGetHealthPercent);


#define shooter_Source_ShootThemUp_Public_UI_STUPlayerHUDWidget_h_15_EVENT_PARMS
#define shooter_Source_ShootThemUp_Public_UI_STUPlayerHUDWidget_h_15_CALLBACK_WRAPPERS
#define shooter_Source_ShootThemUp_Public_UI_STUPlayerHUDWidget_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSTUPlayerHUDWidget(); \
	friend struct Z_Construct_UClass_USTUPlayerHUDWidget_Statics; \
public: \
	DECLARE_CLASS(USTUPlayerHUDWidget, USTUBaseWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ShootThemUp"), NO_API) \
	DECLARE_SERIALIZER(USTUPlayerHUDWidget)


#define shooter_Source_ShootThemUp_Public_UI_STUPlayerHUDWidget_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUSTUPlayerHUDWidget(); \
	friend struct Z_Construct_UClass_USTUPlayerHUDWidget_Statics; \
public: \
	DECLARE_CLASS(USTUPlayerHUDWidget, USTUBaseWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ShootThemUp"), NO_API) \
	DECLARE_SERIALIZER(USTUPlayerHUDWidget)


#define shooter_Source_ShootThemUp_Public_UI_STUPlayerHUDWidget_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USTUPlayerHUDWidget(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USTUPlayerHUDWidget) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USTUPlayerHUDWidget); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USTUPlayerHUDWidget); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USTUPlayerHUDWidget(USTUPlayerHUDWidget&&); \
	NO_API USTUPlayerHUDWidget(const USTUPlayerHUDWidget&); \
public:


#define shooter_Source_ShootThemUp_Public_UI_STUPlayerHUDWidget_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USTUPlayerHUDWidget(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USTUPlayerHUDWidget(USTUPlayerHUDWidget&&); \
	NO_API USTUPlayerHUDWidget(const USTUPlayerHUDWidget&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USTUPlayerHUDWidget); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USTUPlayerHUDWidget); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USTUPlayerHUDWidget)


#define shooter_Source_ShootThemUp_Public_UI_STUPlayerHUDWidget_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__HealthProgressBar() { return STRUCT_OFFSET(USTUPlayerHUDWidget, HealthProgressBar); } \
	FORCEINLINE static uint32 __PPO__PercentColorThreshold() { return STRUCT_OFFSET(USTUPlayerHUDWidget, PercentColorThreshold); } \
	FORCEINLINE static uint32 __PPO__GoodColor() { return STRUCT_OFFSET(USTUPlayerHUDWidget, GoodColor); } \
	FORCEINLINE static uint32 __PPO__BadColor() { return STRUCT_OFFSET(USTUPlayerHUDWidget, BadColor); }


#define shooter_Source_ShootThemUp_Public_UI_STUPlayerHUDWidget_h_12_PROLOG \
	shooter_Source_ShootThemUp_Public_UI_STUPlayerHUDWidget_h_15_EVENT_PARMS


#define shooter_Source_ShootThemUp_Public_UI_STUPlayerHUDWidget_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	shooter_Source_ShootThemUp_Public_UI_STUPlayerHUDWidget_h_15_PRIVATE_PROPERTY_OFFSET \
	shooter_Source_ShootThemUp_Public_UI_STUPlayerHUDWidget_h_15_SPARSE_DATA \
	shooter_Source_ShootThemUp_Public_UI_STUPlayerHUDWidget_h_15_RPC_WRAPPERS \
	shooter_Source_ShootThemUp_Public_UI_STUPlayerHUDWidget_h_15_CALLBACK_WRAPPERS \
	shooter_Source_ShootThemUp_Public_UI_STUPlayerHUDWidget_h_15_INCLASS \
	shooter_Source_ShootThemUp_Public_UI_STUPlayerHUDWidget_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define shooter_Source_ShootThemUp_Public_UI_STUPlayerHUDWidget_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	shooter_Source_ShootThemUp_Public_UI_STUPlayerHUDWidget_h_15_PRIVATE_PROPERTY_OFFSET \
	shooter_Source_ShootThemUp_Public_UI_STUPlayerHUDWidget_h_15_SPARSE_DATA \
	shooter_Source_ShootThemUp_Public_UI_STUPlayerHUDWidget_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	shooter_Source_ShootThemUp_Public_UI_STUPlayerHUDWidget_h_15_CALLBACK_WRAPPERS \
	shooter_Source_ShootThemUp_Public_UI_STUPlayerHUDWidget_h_15_INCLASS_NO_PURE_DECLS \
	shooter_Source_ShootThemUp_Public_UI_STUPlayerHUDWidget_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SHOOTTHEMUP_API UClass* StaticClass<class USTUPlayerHUDWidget>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID shooter_Source_ShootThemUp_Public_UI_STUPlayerHUDWidget_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
