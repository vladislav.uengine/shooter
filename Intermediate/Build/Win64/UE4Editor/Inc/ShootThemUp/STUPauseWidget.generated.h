// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SHOOTTHEMUP_STUPauseWidget_generated_h
#error "STUPauseWidget.generated.h already included, missing '#pragma once' in STUPauseWidget.h"
#endif
#define SHOOTTHEMUP_STUPauseWidget_generated_h

#define shooter_Source_ShootThemUp_Public_UI_STUPauseWidget_h_14_SPARSE_DATA
#define shooter_Source_ShootThemUp_Public_UI_STUPauseWidget_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnClearPause);


#define shooter_Source_ShootThemUp_Public_UI_STUPauseWidget_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnClearPause);


#define shooter_Source_ShootThemUp_Public_UI_STUPauseWidget_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSTUPauseWidget(); \
	friend struct Z_Construct_UClass_USTUPauseWidget_Statics; \
public: \
	DECLARE_CLASS(USTUPauseWidget, USTUBaseWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ShootThemUp"), NO_API) \
	DECLARE_SERIALIZER(USTUPauseWidget)


#define shooter_Source_ShootThemUp_Public_UI_STUPauseWidget_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUSTUPauseWidget(); \
	friend struct Z_Construct_UClass_USTUPauseWidget_Statics; \
public: \
	DECLARE_CLASS(USTUPauseWidget, USTUBaseWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ShootThemUp"), NO_API) \
	DECLARE_SERIALIZER(USTUPauseWidget)


#define shooter_Source_ShootThemUp_Public_UI_STUPauseWidget_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USTUPauseWidget(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USTUPauseWidget) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USTUPauseWidget); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USTUPauseWidget); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USTUPauseWidget(USTUPauseWidget&&); \
	NO_API USTUPauseWidget(const USTUPauseWidget&); \
public:


#define shooter_Source_ShootThemUp_Public_UI_STUPauseWidget_h_14_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USTUPauseWidget(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USTUPauseWidget(USTUPauseWidget&&); \
	NO_API USTUPauseWidget(const USTUPauseWidget&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USTUPauseWidget); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USTUPauseWidget); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USTUPauseWidget)


#define shooter_Source_ShootThemUp_Public_UI_STUPauseWidget_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ClearPauseButton() { return STRUCT_OFFSET(USTUPauseWidget, ClearPauseButton); }


#define shooter_Source_ShootThemUp_Public_UI_STUPauseWidget_h_11_PROLOG
#define shooter_Source_ShootThemUp_Public_UI_STUPauseWidget_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	shooter_Source_ShootThemUp_Public_UI_STUPauseWidget_h_14_PRIVATE_PROPERTY_OFFSET \
	shooter_Source_ShootThemUp_Public_UI_STUPauseWidget_h_14_SPARSE_DATA \
	shooter_Source_ShootThemUp_Public_UI_STUPauseWidget_h_14_RPC_WRAPPERS \
	shooter_Source_ShootThemUp_Public_UI_STUPauseWidget_h_14_INCLASS \
	shooter_Source_ShootThemUp_Public_UI_STUPauseWidget_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define shooter_Source_ShootThemUp_Public_UI_STUPauseWidget_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	shooter_Source_ShootThemUp_Public_UI_STUPauseWidget_h_14_PRIVATE_PROPERTY_OFFSET \
	shooter_Source_ShootThemUp_Public_UI_STUPauseWidget_h_14_SPARSE_DATA \
	shooter_Source_ShootThemUp_Public_UI_STUPauseWidget_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	shooter_Source_ShootThemUp_Public_UI_STUPauseWidget_h_14_INCLASS_NO_PURE_DECLS \
	shooter_Source_ShootThemUp_Public_UI_STUPauseWidget_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SHOOTTHEMUP_API UClass* StaticClass<class USTUPauseWidget>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID shooter_Source_ShootThemUp_Public_UI_STUPauseWidget_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
