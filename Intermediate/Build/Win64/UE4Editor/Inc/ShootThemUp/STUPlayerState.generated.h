// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SHOOTTHEMUP_STUPlayerState_generated_h
#error "STUPlayerState.generated.h already included, missing '#pragma once' in STUPlayerState.h"
#endif
#define SHOOTTHEMUP_STUPlayerState_generated_h

#define shooter_Source_ShootThemUp_Public_Player_STUPlayerState_h_12_SPARSE_DATA
#define shooter_Source_ShootThemUp_Public_Player_STUPlayerState_h_12_RPC_WRAPPERS
#define shooter_Source_ShootThemUp_Public_Player_STUPlayerState_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define shooter_Source_ShootThemUp_Public_Player_STUPlayerState_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASTUPlayerState(); \
	friend struct Z_Construct_UClass_ASTUPlayerState_Statics; \
public: \
	DECLARE_CLASS(ASTUPlayerState, APlayerState, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ShootThemUp"), NO_API) \
	DECLARE_SERIALIZER(ASTUPlayerState)


#define shooter_Source_ShootThemUp_Public_Player_STUPlayerState_h_12_INCLASS \
private: \
	static void StaticRegisterNativesASTUPlayerState(); \
	friend struct Z_Construct_UClass_ASTUPlayerState_Statics; \
public: \
	DECLARE_CLASS(ASTUPlayerState, APlayerState, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ShootThemUp"), NO_API) \
	DECLARE_SERIALIZER(ASTUPlayerState)


#define shooter_Source_ShootThemUp_Public_Player_STUPlayerState_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASTUPlayerState(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASTUPlayerState) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASTUPlayerState); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASTUPlayerState); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASTUPlayerState(ASTUPlayerState&&); \
	NO_API ASTUPlayerState(const ASTUPlayerState&); \
public:


#define shooter_Source_ShootThemUp_Public_Player_STUPlayerState_h_12_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASTUPlayerState(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASTUPlayerState(ASTUPlayerState&&); \
	NO_API ASTUPlayerState(const ASTUPlayerState&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASTUPlayerState); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASTUPlayerState); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASTUPlayerState)


#define shooter_Source_ShootThemUp_Public_Player_STUPlayerState_h_12_PRIVATE_PROPERTY_OFFSET
#define shooter_Source_ShootThemUp_Public_Player_STUPlayerState_h_9_PROLOG
#define shooter_Source_ShootThemUp_Public_Player_STUPlayerState_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	shooter_Source_ShootThemUp_Public_Player_STUPlayerState_h_12_PRIVATE_PROPERTY_OFFSET \
	shooter_Source_ShootThemUp_Public_Player_STUPlayerState_h_12_SPARSE_DATA \
	shooter_Source_ShootThemUp_Public_Player_STUPlayerState_h_12_RPC_WRAPPERS \
	shooter_Source_ShootThemUp_Public_Player_STUPlayerState_h_12_INCLASS \
	shooter_Source_ShootThemUp_Public_Player_STUPlayerState_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define shooter_Source_ShootThemUp_Public_Player_STUPlayerState_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	shooter_Source_ShootThemUp_Public_Player_STUPlayerState_h_12_PRIVATE_PROPERTY_OFFSET \
	shooter_Source_ShootThemUp_Public_Player_STUPlayerState_h_12_SPARSE_DATA \
	shooter_Source_ShootThemUp_Public_Player_STUPlayerState_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	shooter_Source_ShootThemUp_Public_Player_STUPlayerState_h_12_INCLASS_NO_PURE_DECLS \
	shooter_Source_ShootThemUp_Public_Player_STUPlayerState_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SHOOTTHEMUP_API UClass* StaticClass<class ASTUPlayerState>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID shooter_Source_ShootThemUp_Public_Player_STUPlayerState_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
